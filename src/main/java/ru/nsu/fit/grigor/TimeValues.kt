package ru.nsu.fit.grigor

object TimeValues {
    const val expirationTime: Long = 1000
    const val refreshTime: Long = 200
    const val sendTime: Long = 100
}