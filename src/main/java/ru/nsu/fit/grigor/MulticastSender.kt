package ru.nsu.fit.grigor

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.nsu.fit.grigor.TimeValues.expirationTime
import ru.nsu.fit.grigor.TimeValues.sendTime
import java.io.IOException
import java.net.*


class MulticastSender(private val group: InetAddress) {
    private var socket: DatagramSocket? = null
    var buf: ByteArray? = null

    fun startMulticasting() {
        socket = DatagramSocket()

        GlobalScope.launch {
            while (true) {
                buf = "multicast".toByteArray()
                val packet = DatagramPacket(buf, buf!!.size, group, 5555)
                socket!!.send(packet)
                delay(sendTime)
            }
        }
    }
}
