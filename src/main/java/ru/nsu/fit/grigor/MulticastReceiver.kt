package ru.nsu.fit.grigor

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.nsu.fit.grigor.TimeValues.expirationTime
import ru.nsu.fit.grigor.TimeValues.refreshTime
import java.net.DatagramPacket
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.MulticastSocket

class MulticastReceiver(private val group: InetAddress) : Thread() {
    var socket: MulticastSocket? = null
    var buf = ByteArray(256)
    private val groupMemberMap: MutableMap<InetSocketAddress, Long> = mutableMapOf()
    private val scope = CoroutineScope(Dispatchers.Default)

    override fun run() {
        socket = MulticastSocket(5555)
        launchRefreshCoroutine()
        socket?.joinGroup(group)

        while (true) {
            val packet = DatagramPacket(buf, buf.size)
            socket?.receive(packet)
            val some = InetSocketAddress(packet.address, packet.port)
            synchronized(groupMemberMap) {
                if (!groupMemberMap.containsKey(some)) {
                    groupMemberMap[some] = System.currentTimeMillis()
                    updateListView()
                } else {
                    groupMemberMap[some] = System.currentTimeMillis()
                }
            }
        }
    }

    private fun launchRefreshCoroutine() {
        scope.launch {
            while (true) {
                refreshList()
                delay(refreshTime)
            }
        }
    }


    private fun refreshList() {
        synchronized(groupMemberMap) {
            val map = groupMemberMap.filterValues { value -> System.currentTimeMillis() - value < expirationTime }
            if (map != groupMemberMap) {
                groupMemberMap.clear()
                groupMemberMap.putAll(map)
                updateListView()
            }
        }
    }

    private fun updateListView() {
        for (i in 1..6)
            println()

        synchronized(groupMemberMap) {
            groupMemberMap.forEach {
                println(it.key.address.toString() + ":" + it.key.port)
            }
        }
    }
}
