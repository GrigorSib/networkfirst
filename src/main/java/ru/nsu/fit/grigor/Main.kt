package ru.nsu.fit.grigor

import java.net.InetAddress
import java.net.UnknownHostException


fun main(args: Array<String>) {
    if (args.isEmpty()) {
        println("There must two arguments: an IP-address")
        return
    }

    val group = try {
        InetAddress.getByName(args[0])
    } catch (e: UnknownHostException) {
        println(e.localizedMessage)
        return
    }

    val multicastReceiver = MulticastReceiver(group)
    val multicastSender = MulticastSender(group)

    multicastReceiver.start()
    multicastSender.startMulticasting()
}




